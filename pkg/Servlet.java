package Week4_2.src.simpleservlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;



/** This is a simple servlet that implements doPost and doGet.  The doPost takes
 *  two parameters (login and password) and displays them.
 */
@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    /** this is the main method that uses the two parameters and displays them. */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        out.println("<h1>Super Secret Login Information</h1>");
        out.println("<p>Login: " + login + "</p>");
        out.println("<p>Password: " + password + "</p>");
        out.println("</body></html>");
    }
    //run this when Servlet is called
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*
        //create Object
        Person pers = new Person();
        pers.setName("Tyler");
        pers.setPhone(2052460000);

        //covert Object to JSON
        String json = Servlet.personToJSON(pers);
        System.out.println(json);

        //Person pers2 = Servlet.JSONToPerson(json);
        //System.out.println(pers2);
        */

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("testing testing");

    }
    //Method to create Object
    public static String personToJSON(Person person) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        //Exception handling
        try {
            s = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
    //Not used in this Servlet...
    public static Person JSONToPerson(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Person person = null;
        try {
            person = mapper.readValue(s, Person.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return person;
    }



}
