package pkg;
import java.util.*;
public class MyClass {
    @SuppressWarnings("unchecked")
        public static void main(String[] args) {

            System.out.println("-- List -- Non Sorted");
            List<String> ls = new ArrayList<String>();
            ls.add("Andy");
            ls.add("Bob");
            ls.add("Candace");
            ls.add("Daniel");
            ls.add("Edward");
            ls.add("Aaron");
            ls.add("Bob");
            //print out list
            for (Object s : ls) {
                System.out.println((String)s);
            }

            System.out.println("-- Set -- Sorted and removes duplicates");
            Set set = new TreeSet();
            set.add("Andy");
            set.add("Bob");
            set.add("Candace");
            set.add("Daniel");
            set.add("Edward");
            set.add("Aaron");
            set.add("Bob");
            //print out list
            for (Object s : set) {
                System.out.println((String)s);
            }

            System.out.println("-- Queue -- Sorted and doesn't remove duplicates");
            Queue q = new PriorityQueue();
            q.add("Andy");
            q.add("Bob");
            q.add("Candace");
            q.add("Daniel");
            q.add("Edward");
            q.add("Aaron");
            q.add("Bob");
            //create interator
            Iterator itr = q.iterator();
            //print out list
            while (itr.hasNext()) {
                System.out.println(q.poll());
            }

            System.out.println("-- Map -- Manually order objects and overwrite");
            Map m = new HashMap();
            m.put(1,"Andy");
            m.put(2,"Bob");
            m.put(3,"Candace");
            m.put(4,"Daniel");
            m.put(5,"Edward");
            m.put(3,"Aaron");
            m.put(6,"Bob");
            //print out list
            for (int i = 1; i < 7; i++) {
                String r = (String)m.get(i);
                System.out.println(r);
            }

            System.out.println("-- Comparator Interface --");
            List ls1 = new ArrayList();
            ls1.add("Andy");
            ls1.add("Bob");
            ls1.add("Candace");
            ls1.add("Daniel");
            ls1.add("Edward");
            ls1.add("Aaron");
            ls1.add("Bob");
            //sort list
            Collections.sort(ls1);
            //print out list
            System.out.println(ls1);


            System.out.println("-- List using Generics --");
            //get jobs and list from other class
            List<jobs> myList = new LinkedList<jobs>();
            myList.add(new jobs("Tyler", "Systems Admin"));
            myList.add(new jobs("Jason", "Systems Admin Lead"));
            myList.add(new jobs("Mary", "Human Resources"));
            myList.add(new jobs("Brandon", "Systems Admin"));
            //print out list
            for (jobs j : myList) {
                System.out.println(j);
            }
        }
}
