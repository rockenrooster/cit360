package pkg;
public class jobs {
    private String Name;
    private String JobTitle;
    public jobs(String Name, String JobTitle) {
        this.Name = Name;
        this.JobTitle = JobTitle;
    }
    public String toString() {
        return "Name: " + Name + " -- Job Title: " + JobTitle;
    }
}
