package pkg;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Client {
    //Method to create Client instance
    public static String getHttpContent(String string) {
        //make content not null to prevent error
        String content="";
        //Error handling
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();
            //Error handling (not very specific, but anything can happen with HTTP)
        } catch (Exception e) {
            System.out.println("Error connecting to Server, Check network connection, is Server running?");
        }
        return content;
    }

    //Main method to start Client method and grab data from Server
    public static void main(String[] args) {
       String s = Client.getHttpContent("http://localhost:8000/test");

       System.out.println("JSON from Server: " + s);

       System.out.println("Converted to Object: " + JSONToPerson(s));

    }
    //method to convert JSON to Person Object, (Server doesn't need this)
    public static Person JSONToPerson(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Person person = null;
        //Error handling
        try {
            person = mapper.readValue(s, Person.class);
        } catch (JsonProcessingException e) {
            System.out.println("Error Converting JSON String to Object");
        }
        return person;
    }
}

