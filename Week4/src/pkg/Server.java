package pkg;
//Http server code from https://stackoverflow.com/questions/3732109/simple-http-server-in-java-using-only-java-se-api
//Modified to work with this project
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Server {
    //create http server on port 8000
    public static void main(String[] args) {

        //Error handling, (Generic since anything could happen with HTTP server. Port in use, no network, etc)
        try{
            HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
            //make localhost:8000/test the default executor that gets called by client
            server.createContext("/test", new MyHandler());
            server.setExecutor(null); // creates a default executor
            server.start();
            }
         catch (Exception e) {
             System.out.println("Error creating HTTP Server, Check Network connection and that Port 8000 isn't in use already");
        }



    }
    //handler to deliver json object to Client via JSON string
    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            //create Object
            Person pers = new Person();
            pers.setName("Tyler");
            pers.setPhone("2052460000");

            //covert Object to JSON
            String json = personToJSON(pers);
            System.out.println("Data read from Server: " + json);
            //assign JSON string to response string
            String response = json;

            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    //Method to create Object. (Server doesn't need JSONtoPerson)
    public static String personToJSON(Person person) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        //Exception handling
        try {
            s = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            System.out.println("Error Converting Object to JSON String");
        }

        return s;
    }

}
//Person class is a part of Server.java to keep things organized into just 2 files.
// Client doesn't need this class anyways...
class Person {

    private String name;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String toString() {
        return "Name: " + name + " Phone: " + phone;
    }


}
