package pkg;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Week3_1 {
    private static int result;

    public static void main(String[] args) {
        String num1;
        String num2;


        //method to grab input from console
        Scanner input = new Scanner(System.in);
        //perpetually re run program until user puts in non zero number for second input
        boolean working = false;
        while (!working) {

            //prompt for user input
                System.out.print("Enter a number\n");
                num1 = input.next();
                System.out.print("Enter a second number\n");
                num2 = input.next();




            try {
                //Exception Handling
                int in1 = Integer.parseInt(num1);
                int in2 = Integer.parseInt(num2);
                System.out.println(doDivision(in1, in2));
                working = true;
            }


            //exception handling
            catch (InputMismatchException | ArithmeticException | NumberFormatException e) {

                System.out.println("Please Enter a non-zero number");

            }
        }
        input.close();

        }




    //Method for Division and throw handling
    private static String doDivision(int num1, int num2) throws InputMismatchException, ArithmeticException {

            //Math
            result = num1 / num2;
            return ("" + num1 + " Divided by " + num2 + " is " + result);
            //display result to user

    }

}












