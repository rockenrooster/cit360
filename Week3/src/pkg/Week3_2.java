package pkg;

import java.util.Scanner;

public class Week3_2 {
    private static int result;

    public static void main(String[] args) {
        //method to grab input from console
        Scanner input = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;
        //prompt for user input

        //Data validation: Make sure input isn't a letter
        boolean working =false;
        while (!working) {
        try{
            System.out.print("Enter a number\n");
            num1 = Integer.parseInt(input.next());
            working = true;
            }
        catch(NumberFormatException e){
                System.out.print("must be a non zero number\n");
                System.out.print("try again\n");
            }
        }



        //Data validation: Make sure input isn't a letter
        boolean working2 =false;
        while (!working2 && num2 == 0) {
            try{
                System.out.print("Enter a second number\n");
                num2 = Integer.parseInt(input.next());
                //call doDivision Method
                System.out.println(doDivision(num1, num2));
                working2 = true;
            }
            catch(NumberFormatException | ArithmeticException e){
                System.out.print("must be a non zero number\n");
                System.out.print("try again\n");
            }
        }

        //Close input scanner
        input.close();
    }


    //Method for Division  and throw handling
    private static String doDivision(int num1, int num2) throws ArithmeticException{

            //Math
            result = num1 / num2;
            return ("" + num1 + " Divided by " + num2 + " is " + result);
            //display result to user

    }

}












