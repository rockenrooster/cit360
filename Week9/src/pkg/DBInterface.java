package pkg;

//Search org.hibernate for jar files needed and import them to IDE
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;

//Class used to get Database Data
public class DBInterface {

    //Init sess and fact variables
    Session sess = null;
    SessionFactory fact;

    //Init instance object
    private static DBInterface instance = null;

    //Method for DBInterface to get Session Factory
    private DBInterface()
    {
        fact = HibernateSF.getSf();
    }

    //Method to get instance
    public static DBInterface getInstance()
    {
        if (instance == null) {
            instance = new DBInterface();
        }
        return instance;
    }

    //Method to print out Database Data
    public List<Car> getCars() {
        try {
            sess = fact.openSession();
            sess.getTransaction().begin();
            String sql = "from pkg.Car";
            List<Car> cars = (List<Car>) sess.createQuery(sql).getResultList();
            sess.getTransaction().commit();
            sess.close();

            //return cars from DB
            return cars;
        }
        catch (Exception e) {
            System.out.println("An Error Occurred. Rolling back");

            //Start Rollback
            sess.getTransaction().rollback();
            sess.close();
            return null;
        }
    }
}
