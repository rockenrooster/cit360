package pkg;

//Search org.hibernate for jar files needed and import them to IDE
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

//Sets up the session factory used by DBInterface
public class HibernateSF {

    private static final SessionFactory sf = makeSessionFactory();

    //Build session factory
    private static SessionFactory makeSessionFactory() {
        try {
            //Use hibernate.cfg.xml
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();

            //Create metadata
            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();
        }
        catch (Exception e) {
            System.out.println("SessionFactory creation failed." + e);
            return null;
        }
    }

    public static SessionFactory getSf() {
        return sf;
    }

    public static void shutdown() {
        getSf().close();
    }
}

