package pkg;

import org.hibernate.Session;
import java.util.List;
import java.util.Scanner;

public class RunHibernate {
    //main method and print output
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        boolean done = false;
        while (!done) {
            //prompt for user input
            System.out.print("Press 1 to get Database Data\nPress 2 to add Database Data\nPress 3 to close program\n");

            //3 options for program to run
            try {
                int num1 = Integer.parseInt(input.next());

                if (num1 == 1) {
                    getData();
                }
                else if (num1 == 2) {
                    addData();
                }
                else if (num1 == 3) {
                    done = true;
                }
                else {
                    System.out.println("Error");
                }
            }
            catch(NumberFormatException e){
                System.out.print("must be a number\n");
                System.out.print("try again\n");
            }
        }
    }

    public static void getData() {
        DBInterface t = DBInterface.getInstance();
        List<Car> c = t.getCars();
        for (Car i : c) {
            System.out.println(i);
        }
    }

    public static void addData() {

        //Init car variables
        String year;
        String make;
        String model;

        Scanner input = new Scanner(System.in);

        //Enter Name
        System.out.println("Enter Year");
        year = input.next();

        //Enter Address
        System.out.println("Enter Make");
        make = input.next();

        //Enter Phone
        System.out.println("Enter Model");
        model = input.next();

        Session s = HibernateSF.getSf().openSession();
        s.beginTransaction();

        //Add new Car object
        Car car = new Car();

        //Set values to user input
        car.setYear(year);
        car.setMake(make);
        car.setModel(model);

        //Save car in database
        s.save(car);

        //Save/Commit DB
        s.getTransaction().commit();

        getData();
        HibernateSF.shutdown();
    }
}


