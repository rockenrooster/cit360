package pkg;

import javax.persistence.*;
import java.io.Serializable;

//Data object class to Car Table in Database
@Entity
@Table(name = "car", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})
public class Car implements Serializable {

    private static final long serialVersionUID = -1798070786993154676L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "year")
    private String year;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    //getters and setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }
    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

        //Output to console and add space to end
    public String toString() {
        return "-id: "+ Integer.toString(id) + "\n-year: " + year + "\n-make: " + make + "\n-model: " + model +"\n";
    }
}
