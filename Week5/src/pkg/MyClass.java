package pkg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//this class contains methods used by the Junit tests
//Some tests call the same method more than once.
public class MyClass {

    //return sum
    public int addNumbers(int num1, int num2) {
        int result = 0;

        //Simple Error handling in case not a number
        try{
            result = num1+num2;
        }
        catch(NumberFormatException e) {
            System.out.println("Must be a non zero number!");
        }


        return result ;
    }

    //crate arraylist
    public List Array(){
        List<String> array = new ArrayList<>();
        return array;
    }

    //method boolean greater than
    public boolean greaterThan(int num1,int num2) {
        //Simple Error handling in case not a number
        try{
            if (num1>num2){ return true; }
            else { return false; }
        }
        catch(NumberFormatException e) {
            System.out.println("Please Enter non-zero numbers!");
            return false;
        }
    }

    //method boolean lessthan
    public boolean lessThan(int num1,int num2) {
        //Simple Error handling in case not a number
        try{
            if (num1<num2){ return true; }
            else { return false; }
        }
        catch(NumberFormatException e) {
            System.out.println("Please Enter non-zero numbers!");
            return false;
        }
    }

    //hashmap test
    public String assertNotNullTest(final String key) {
        Map<String, String> map = new HashMap<>();
        map.put("One","1");
        map.put("Two","2");
        map.put("null", null);
        return map.get(key);
    }

    //hashmap test2
    public String assertNotSameTest(final String key) {
        Map<String,String> map=new HashMap<>();
        map.put("1","One");
        map.put("2","Two");
        map.put("3","Three");
        map.put("4","Four");
        return map.get(key);
    }
}