package pkg;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

        //Junit tests class
public class Junit {

    //make sure Array is what is expected. expected == actual
    @Test
    void Array() {
        String[] expected= {"One","Two","Three"};
        String[] actual={"One","Two","Three"};
        assertArrayEquals(expected,actual);
    }

    //test to see if result matches
    @Test
    void addingTest() {
        MyClass JUnit = new MyClass();
        int expected =10;

        int actual= JUnit.addNumbers(4, 6);

        assertEquals(expected, actual);
    }

    //make sure result is false
    @Test
    void greaterThan() {
        MyClass JUnitBool= new MyClass();
        assertFalse(JUnitBool.greaterThan(3,4));
    }

    // make sure keys have value and not null
    @Test
    void assertNotNullTest() {
        MyClass JUnitHashMap= new MyClass();
        assertNotNull(JUnitHashMap.assertNotNullTest("One"));
    }

    //make sure each key references a diff string
    @Test
    void assertNotSameTest() {
        MyClass JUnit= new MyClass();
        assertNotSame(JUnit.assertNotSameTest("1"),JUnit.assertNotSameTest("2"));
    }

    //make sure it is equal to null
    @Test
    void assertNullTest() {
        MyClass JUnitHashMap= new MyClass();
        assertNull(JUnitHashMap.assertNotNullTest("null"));
    }

    //Make sure they are the same
    @Test
    void assertSameTest() {
        MyClass JUnit= new MyClass();
        assertSame(JUnit.assertNotSameTest("1"),JUnit.assertNotSameTest("1"));
    }

    //make sure less than boolean and assert true
    @Test
    void lessThan() {
        MyClass JUnitLessBool = new MyClass();
        assertTrue(JUnitLessBool.lessThan(3,4));
    }
}
