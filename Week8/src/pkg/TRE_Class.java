package pkg;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class TRE_Class {

    private static int num;


    public static void main(String[] args) throws InterruptedException {

        try {
            //show user prompt to start thread
            System.out.print("Press Enter to start Thread for 1 millisecond\n");
            promptEnterKey();
            num = 1;
            //start thread
            thread();

            //show user prompt to start runnable
            System.out.print("Press Enter to start 2 Runnables\n");
            promptEnterKey();
            //start runnable
            runnable1();

            //show user prompt to start Executor runnables
            System.out.print("Press Enter to start 5 Runnables using an Executor\n");
            promptEnterKey();
            //start runnable
            runnable2();
        }
        catch (InterruptedException e)
        {
            System.out.print("An Error occurred please restart program\n");
        }

    }
    //Data Validation: make sure user can't mess up program by putting in letters numbers symbols etc.
    //Method to allow user to just press the Enter key to continue instead requiring some kind of input
    public static void promptEnterKey(){

        try{
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
        }catch(Exception e){
            System.out.print("An error occurred, please restart program\n");
        }

    }

    //method to create a thread
    private static void thread() throws InterruptedException {
        //Create Atomic Integer that threads can manipulate
        AtomicInteger count = new AtomicInteger();
        Thread t1 = new Thread(() -> {
            int i = 0;
            while (i == 0)
                count.incrementAndGet();
        });
        t1.start();
        Thread.sleep( num );
        t1.interrupt();
        System.out.println("Counted " + count + " times in "+num+" miliseconds");
        t1.stop();
    }
    //method to create runnable object and start runnable object
    private static void runnable1() throws InterruptedException {

        MyRunnable r1 = new MyRunnable(num);
        //r1.run();
        Thread t1 = new Thread(r1);
        t1.start();

        num++;

        MyRunnable r2 = new MyRunnable(num);
        //r2.run();
        Thread t2 = new Thread(r2);
        t2.start();

    }

    private static void runnable2() throws InterruptedException {
        //create Executor using 3 threads max
        ExecutorService myExec = Executors.newFixedThreadPool(3);

        //create runnables
        MyRunnable r1 = new MyRunnable(1);
        MyRunnable r2 = new MyRunnable(2);
        MyRunnable r3 = new MyRunnable(3);
        MyRunnable r4 = new MyRunnable(4);
        MyRunnable r5 = new MyRunnable(5);

        //assign runnables to Executors
        myExec.execute(r1);
        myExec.execute(r2);
        myExec.execute(r3);
        myExec.execute(r4);
        myExec.execute(r5);

        myExec.shutdown();


    }


}




class MyRunnable implements Runnable{

    private int s;
    public MyRunnable(int s) {
        this.s = s;
    }
    public void run() {
        System.out.println(s+ " Runnable");

    }
}