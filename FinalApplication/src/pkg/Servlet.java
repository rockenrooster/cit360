package pkg;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


//http://localhost:8081/FinalApplication_Web_exploded/calc.html is the default page to enter info

//http://localhost:8081/FinalApplication_Web_exploded/Servlet is the URL that will run this class



@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {

    //create Object to hold numbers and operand
    Calc calc = new Calc();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        //Thread to benchmark calc
        AtomicInteger count = new AtomicInteger();
        Thread t2 = new Thread(() -> {
            int i = 0;
            while (i == 0)
                count.incrementAndGet();
        });
        t2.start();

        //Set type to HTML page
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        //Get input from User and exception handling
        try {

            //get text that was entered and assign to Object values
            double num1 = (Double.parseDouble(request.getParameter("num1")));
            String op = (request.getParameter("symbol"));
            double num2 = (Double.parseDouble(request.getParameter("num2")));


            out.println("<h1>Servlet Calculator</h1>");
            out.println(thread(request, response, out, num1, num2, op)); //This is where processing is done.
            out.println("<a href=\"http://localhost:8081/FinalApplication_Web_exploded/calc.html\"><p>Next Calculation?</p> </a>");
            t2.interrupt();
            out.println("<p>CPU Cycles Waited: " + count); //cute little benchmark to show how many CPU cycles have passed while calculation was done, higher number is better when comparing computers doing the same calculation (I think)

            out.println("<h2>History: </h2>");

            try (BufferedReader br = new BufferedReader(new FileReader("../../calcTest.json"))) {
                String line;
                while ((line = br.readLine()) != null) {
                    // process the line.
                    String text = JSONToCalc(line).toString();
                    out.print("<p> "+ text +" </p>");
                }
            }



            out.println("</body></html>");

        } catch(NumberFormatException | InterruptedException e){

            out.println("<h1>Error! Please Enter valid Digits! (0-9)</h1>");
            out.println("<a href=\"http://localhost:8081/FinalApplication_Web_exploded/calc.html\"><p>Try Again</p> </a>");
            out.println("</body></html>");
            t2.interrupt();
        }

        //Close Servlet
        response.getOutputStream().close();
        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Something went wrong.");
    }



    public <AtomicDouble> String thread(HttpServletRequest request, HttpServletResponse response, PrintWriter out, double num1, double num2, String operand) throws InterruptedException, IOException {

        //Create Atomic variable that threads can manipulate
        String resultString;
        AtomicReference<Double> result = new AtomicReference<>((double)0);
        AtomicReference<String> wording = new AtomicReference<>("null");
        AtomicReference<Double> valid = new AtomicReference<>((double)0);

        //create Thread processing calculation
        Thread t2 = new Thread(() -> {

                //get operand and do calculation
                switch (operand) {
                    case "+":
                        result.set(num1 + num2);
                        wording.set("plus");
                        valid.set((double) 1);
                        break;
                    case "-":
                        result.set(num1 - num2);
                        wording.set("minus");
                        valid.set((double) 1);
                        break;
                    case "*":
                        result.set(num1 * num2);
                        wording.set("times");
                        valid.set((double) 1);
                        break;
                    case "/":
                        result.set(num1 / num2);
                        wording.set("divided by");
                        valid.set((double) 1);
                        break;
                    //If operand is invalid set result to -1 to trigger the IF statement below
                    default:
                        valid.set((double) 0);
                }


                calc.setNum1(new BigDecimal(num1).stripTrailingZeros().toPlainString());
                calc.setOperand(operand);
                calc.setNum2(new BigDecimal(num2).stripTrailingZeros().toPlainString());
                calc.setResult(new BigDecimal(String.valueOf(result)).stripTrailingZeros().toPlainString());

            //Exception handling
            try {

                ToJSON(calc);//Write JSON output to file

            } catch (IOException e) {
                System.err.println("Problem writing to the file calcTest.txt");
            } catch (Exception e) {
                System.out.println("<h1>Error</h1>");
            }

        });

        t2.start();
        t2.join(); //wait for thread to complete

        //Data Validation
        if(valid.get() == 0){
            resultString="<h1>Error! Please Enter a valid Operand! ('+' '-' '*' or '/'.</h1>";
        }
        else {
            //remove trailing zeros
            String number1 = new BigDecimal(num1).stripTrailingZeros().toPlainString();
            String number2 = new BigDecimal(num2).stripTrailingZeros().toPlainString();
            String resultNum = new BigDecimal(String.valueOf(result)).stripTrailingZeros().toPlainString();

            resultString="<p>Result: " + number1 + " " + wording + " " + number2 + " is " + resultNum + "</p>";
        }



        return resultString;


    }

    public static void ToJSON(Calc calc) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String s = mapper.writeValueAsString(calc);

        //Create a text file with the Calculation history
        //File statText = new File("../../calcTest.json");
        FileWriter fw = new FileWriter("../../calcTest.json", true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(s);
        bw.newLine();
        bw.close();
    }


    public static Calc JSONToCalc(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Calc calc = null;
        //Error handling
        try {
            calc = mapper.readValue(s, Calc.class);
        } catch (JsonProcessingException e) {
            System.out.println("Error Converting JSON String to Object");
        }
        return calc;
    }
}


class Calc {

    private String num1;
    private String num2;
    private String operand;
    private String result;


    public String getNum1() {
        return num1;
    }

    public void setNum1(String num1) {
        this.num1 = num1;
    }

    public String getNum2() {
        return num2;
    }

    public void setNum2(String num2) {
        this.num2 = num2;
    }

    public String getOperand() {
        return operand;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public String toString() {
        return "FirstNumber" + " : " + num1 + ", " + " Operand" + " : " + operand + ", " + " SecondNumber" + " : " + num2 + ", " + " Result" + " : " + result;
    }

}