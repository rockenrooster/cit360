package pkg;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// http://localhost:8080/Week7_Web_exploded/login.html is the default page to enter info

// http://localhost:8080/Week7_Web_exploded/Servlet is the URL that will run this class



@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //open Print Writer object to use
        PrintWriter out = response.getWriter();

        //Set type to HTML page
        response.setContentType("text/html");
        out.println("<html><head></head><body>");

        //get text that was entered and assign to String
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");

        //Print Strings to HTML page with info that user Entered
        out.println("<h1>Name Info</h1>");
        out.println("<p>First Name: " + firstName + "</p>");
        out.println("<p>Last Name: " + lastName+ "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Something went wrong.");
    }






}
